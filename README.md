# predica-nrf52-task

Proszę przygotować program na płytkę deweloperską "nRF52 DK", który w momencie wciśnięcia
przycisku "BUTTON 3" uruchomi wyszukiwanie urządzenia BLE pod nazwą "Senior-Predica" i ustanowi z
nim połączenie BLE (jeśli zostanie wykryte). Jeżeli po 10s nie nastąpi wykrycie urządzenia o podanej
nazwie to skanowanie zostaje wyłączone.

Ponadto każdy stan urządzenia powinien zostać zwizualizowany przy pomocy diody LED 3:

- w trybie bez skanowania dioda powinna migać z częstotliwością 1Hz,

- w trybie skanowania z częstotliwością 2Hz,

- w momencie udanego połączenia z urządzeniem "Senior-Predica" powinna zacząć migać z
częstotliwością 5Hz,

- w momencie gdy urządzenie nie zostanie wykryte w przeciągu 10s, dioda powraca do migania z
częstotliwością 1Hz,

- jeśli nie uda się poprawnie połączyć z urządzeniem, powinna migać z częstotliwością 0,5Hz.

Należy stworzyć stałą DevName w pamięci nieulotnej programu, która będzie przechowywała
niezmienną nazwę urządzenia.

Prośba aby kod opatrzeć komentarzem z objaśnieniem logiki działania.