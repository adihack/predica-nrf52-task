#ifndef APP_PREDICA_H
#define APP_PREDICA_H

#include "app_timer.h"
#include "app_button.h"

#include "boards.h"
#include "bsp.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_conn_params.h"
#include "nrf_ble_scan.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_pwr_mgmt.h"

extern nrf_ble_scan_t *m_scan_ptr;

#define PREDICA_LED                     BSP_BOARD_LED_0 // ASO: zgodnie z zadaniem, status zwracam wylacznie za pomoca jednej diody

#define PREDICA_BUTTON_PIN              BUTTON_3 // ASO: Przycisk 3
#define PREDICA_BUTTON_DEBOUNCE         APP_TIMER_TICKS(50) // ASO: minimalny czas wcisiniecia przycisku - zabezpiecza przed niepozadanymi wywolaniami z powodu charakterstyki mechanicznego przycisku

static enum deviceStateEnum {
  DEVICE_STATE_IDLE,
  DEVICE_STATE_SCANNING,
  DEVICE_STATE_CONNECTED,
  DEVICE_STATE_CONNECTION_ERROR
} currentDeviceState;

void handleLed();
void scan_start(void);
void scan_stop(void);
void set_device_connected();
void set_device_connection_error();
void set_device_idle();
void led_blink_timer_handler(void * p_context);
void create_timers();
void button_event_handler(uint8_t pin_no, uint8_t button_action);

#endif // APP_PREDICA_H