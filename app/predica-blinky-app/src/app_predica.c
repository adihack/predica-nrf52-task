#include "app_predica.h"

nrf_ble_scan_t *m_scan_ptr;                     // wkaznik na obiekt m_scan

APP_TIMER_DEF(m_predica_led_blink_timer);       // ASO: instancja timera osblugujacego miganie diody

/** brief Funkcja ustawia timer sterujacy miganiem diody na podstawie aktualnego stanu urzadzenia(czesc wykonawcza maszyny stanu)
*/
void handleLed()
{
  app_timer_stop(m_predica_led_blink_timer);
  switch(currentDeviceState) {
    case DEVICE_STATE_IDLE:
      app_timer_start(m_predica_led_blink_timer, APP_TIMER_TICKS(500), NULL); // ASO: miganie 1 hz
      break;
    case DEVICE_STATE_SCANNING:
      app_timer_start(m_predica_led_blink_timer, APP_TIMER_TICKS(250), NULL); // ASO: miganie 2 hz
      break;
    case DEVICE_STATE_CONNECTED:
      app_timer_start(m_predica_led_blink_timer, APP_TIMER_TICKS(100), NULL); // ASO: miganie 5 hz
      break;
    case DEVICE_STATE_CONNECTION_ERROR:
      app_timer_start(m_predica_led_blink_timer, APP_TIMER_TICKS(1000), NULL); // ASO: miganie 0.5 hz
      break;
    default:
      break;
  }
}


/**@brief Funkcja rozpoczyna skanowanie BLE
 */
void scan_start(void)
{
    nrf_ble_scan_start(m_scan_ptr);
    
    currentDeviceState = DEVICE_STATE_SCANNING;
    handleLed();
}

/**@brief Funkcja konczy skanowanie BLE i przechodzi do stanu IDLE
 */
void scan_stop(void)
{
    nrf_ble_scan_stop();
  
    currentDeviceState = DEVICE_STATE_IDLE;
    handleLed();
}

/**@brief Funkcja konczy skanowanie BLE i przechodzi do polaczany z urzadzeniem
 */
void set_device_connected()
{
    nrf_ble_scan_stop();
    
    currentDeviceState = DEVICE_STATE_CONNECTED;
    handleLed();
}

/**@brief Funkcja konczy skanowanie BLE i przechodzi do stanu blad polaczenia z urzadzeniem
 */
void set_device_connection_error()
{
    nrf_ble_scan_stop();
    
    currentDeviceState = DEVICE_STATE_CONNECTION_ERROR;
    handleLed();
}

/**@brief Funkcja konczy skanowanie BLE i przechodzi do stanu spoczynku
 */
void set_device_idle()
{
    nrf_ble_scan_stop();
    
    currentDeviceState = DEVICE_STATE_IDLE;
    handleLed();
}

/**@brief Funkcja callbacka zmieniajacego stan diody
 */
void led_blink_timer_handler(void * p_context)
{   
    bsp_board_led_invert(PREDICA_LED);
}

/**@brief Funkcja tworzaca timer sterujacy dioda
 */
void create_timers()
{   
    app_timer_create(&m_predica_led_blink_timer,APP_TIMER_MODE_REPEATED,led_blink_timer_handler);
}


/**@brief Funkcja callbacka obslugujaca zdarzenia przycisku
 *
 * @param[in] pin_no        numer pinu ktorego dotyczy zdarzenie
 * @param[in] button_action typ ackji przycisku (press/release)
 */
void button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    switch (pin_no)
    {            
        case PREDICA_BUTTON_PIN:
          scan_start();
          break;
        default:
            break; // pozostale przyciski ignorujemy
    }
}
